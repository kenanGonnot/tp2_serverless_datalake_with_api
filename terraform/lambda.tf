# TODO : Create the lambda role with aws_iam_role
# TODO : Create lambda function with aws_lambda_function
# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role
# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment
# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment
# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "AmazonS3FullAccess"
    }
  ]
}
EOF
}

#resource "aws_lambda_function" "test_lambda" {
#  filename      = "empty_lambda_code.zip"
#  handler       = "lambda_main_app.lambda_handler"
#  statement_id  = "AllowExecutionFromS3Bucket"
#  action        = "lambda:InvokeFunction"
#  runtime       = "python3.7"
#  function_name = "lambda_function_name"
#  role          = aws_iam_role.iam_for_lambda.arn
#  source_arn    = aws_s3_bucket.bucket.arn
#  principal     = "s3.amazonaws.com"
#
#
#
#  # The filebase64sha256() function is available in Terraform 0.11.12 and later
#  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
#  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
#  source_code_hash = filebase64sha256("lambda_function_payload.zip")
#
##  file_system_config {
##    # EFS file system access point ARN
##    arn = aws_efs_access_point.access_point_for_lambda.arn
##
##    # Local mount path inside the lambda function. Must start with '/mnt/'.
##    local_mount_path = "/mnt/efs"
##  }
#
##  vpc_config {
##    # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
##    subnet_ids         = [aws_subnet.subnet_for_lambda.id]
##    security_group_ids = [aws_security_group.sg_for_lambda.id]
##  }
#
#  # Explicitly declare dependency on EFS mount target.
#  # When creating or updating Lambda functions, mount target must be in 'available' lifecycle state.
##  depends_on = [aws_efs_mount_target.alpha]
##}
#
#
#  environment {
#    variables = {
#      foo = "bar"
#    }
#  }
#}


resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.data_processing_esme.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3-bucket.arn
}

resource "aws_lambda_function" "data_processing_esme" {
  filename      = "empty_lambda_code.zip"
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  function_name = "lambda_data_processing"
  role          = aws_iam_role.iam_for_lambda.arn
}


resource "aws_iam_policy" "policy" {
  name        = "test_policy"
  path        = "/"
  description = "My test policy"


  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:Describe*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}


resource "aws_cloudwatch_log_group" "example"{
  name = "/aws/lambda/${aws_lambda_function.data_processing_esme.function_name}"
  retention_in_days = 14
}

resource "aws_iam_role_policy_attachment" "AmazonS3FullAccess" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
