# TODO : Create a s3 bucket with aws_s3_bucket
# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object
# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket" "s3-bucket" {
  bucket = "s3-job-offer-bucket-kg"
  force_destroy = true


#  tags = {
#    Name        = "My bucket"
#    Environment = "Dev"
#  }
}

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.s3-bucket.bucket
  key = "job_offers/raw/"
  source = "/dev/null"
#  content_type = "text/html"
}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3-bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.data_processing_esme.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}




